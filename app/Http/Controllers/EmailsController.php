<?php

namespace App\Http\Controllers;

use App\Rules\Phone;
use App\Mail\Contacted;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\Email as EmailResource;
use App\Repositories\EmailRepositoryInterface;

class EmailsController extends Controller
{
    /**
     * The Email Repository instance.
     *
     * @var EmailRepository
     */
    protected $email;


    /**
     * EmailControllers constructor.
     *
     * @param EmailRepositoryInterface $email
     * @return void
     */
    public function __construct(EmailRepositoryInterface $email)
    {
        $this->email = $email;
    }

    /**
     * Creates an Email resource and emails Guy Smiley.
     *
     * @param   Request $request
     * @return  Email
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => ['nullable', new Phone],
            'message' => 'required',
        ]);

        $email = $this->email->create($request->all());

        Mail::to(env('CONTACT_TO', 'guy-smiley@example.com'))->send(new Contacted($email));

        return (new EmailResource($email))->additional([
                    'message' => 'Guy Smiley is now one smiley guy.',
                ]);
    }
}
