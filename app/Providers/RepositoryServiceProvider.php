<?php

namespace App\Providers;

use App\Repositories\EmailRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\EmailRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind repository interfaces to implementations.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmailRepositoryInterface::class, EmailRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
