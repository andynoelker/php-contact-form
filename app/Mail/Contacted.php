<?php

namespace App\Mail;

use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contacted extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The email instance.
     *
     * @var Email
     */
    public $email;

    /**
     * Create a new message instance.
     *
     * @param Email $email
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contacted')
                    ->from(env('CONTACT_FROM', 'andy@example.com'))
                    ->subject('You have a new message');

    }
}
