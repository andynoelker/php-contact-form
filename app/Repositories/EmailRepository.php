<?php

namespace App\Repositories;

use App\Models\Email;

class EmailRepository implements EmailRepositoryInterface
{
    /**
     * The Email model.
     *
     * @var Email
     */
    protected $model;

    /**
     * EmailRepository contructor.
     *
     * @param Email $email
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->model = $email;
    }

    /**
     * Creates an Email resource from a provided set of attributes.
     *
     * @param array $attributes
     * @return Email
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
}
