<?php

namespace App\Repositories;

interface EmailRepositoryInterface
{
    public function create(array $attributes);
}
