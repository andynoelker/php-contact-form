<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Mail\Contacted;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp()
    {
        parent::setUp();

        // Make sure no mail actually goes out
        Mail::fake();
    }

    /** @test */
    public function user_can_submit_contact_email()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->numerify('###-###-####'),
            'message' => $this->faker->text,
        ];

        $response = $this->json('POST', '/api/emails', $data);

        $response
            ->assertStatus(201)
            ->assertJson([
                'status' => 'success',
            ]);
    }

    /** @test */
    public function phone_number_is_optional()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'message' => $this->faker->text,
        ];

        $response = $this->json('POST', '/api/emails', $data);

        $response
            ->assertStatus(201)
            ->assertJson([
                'status' => 'success',
            ]);
    }

    /** @test */
    public function validation_stops_bad_contact_data()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->name,
            'phone' => $this->faker->name,
            'message' => $this->faker->text,
        ];

        $response = $this->json('POST', '/api/emails', $data);

        $response->assertStatus(422);
    }

    /** @test */
    public function stores_an_email_record()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->numerify('###-###-####'),
            'message' => $this->faker->text,
        ];

        $response = $this->json('POST', '/api/emails', $data);

        $response
            ->assertJson([
                'data' => $data,
            ]);

        $this->assertDatabaseHas('emails', $data);
    }

    /** @test */
    public function emails_guy_smiley()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->numerify('###-###-####'),
            'message' => $this->faker->text,
        ];

        $response = $this->json('POST', '/api/emails', $data);

        Mail::assertSent(Contacted::class, function ($mail) use ($data) {
            return $mail->hasTo(env('CONTACT_TO', 'guy-smiley@example.com')) &&
                $mail->email->name === $data['name'] &&
                $mail->email->email === $data['email'] &&
                $mail->email->phone === $data['phone'] &&
                $mail->email->message === $data['message'];
        });
    }
}
