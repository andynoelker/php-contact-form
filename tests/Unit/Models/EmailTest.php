<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Email;
use Illuminate\Foundation\Testing\WithFaker;
use App\Repositories\EmailRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmailTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @var EmailRepository
     */
    protected $email;

    public function setUp()
    {
        parent::setUp();

        $this->email = resolve(EmailRepositoryInterface::class);
    }

    /** @test */
    public function it_can_create_an_email()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'message' => $this->faker->text,
        ];

        $email = $this->email->create($data);

        $this->assertInstanceOf(Email::class, $email);
        $this->assertEquals($data['name'], $email->name);
        $this->assertEquals($data['email'], $email->email);
        $this->assertEquals($data['phone'], $email->phone);
        $this->assertEquals($data['message'], $email->message);
    }
}
