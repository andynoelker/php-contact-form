<?php

namespace Tests\Unit\Rules;

use Tests\TestCase;
use App\Rules\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PhoneTest extends TestCase
{
    /**
     * @var Phone
     */
    protected $rule;

    public function setUp()
    {
        parent::setUp();

        $this->rule = new Phone();
    }

    /** @test */
    public function it_passes_valid_phone_numbers()
    {
        $validNumbers = [
            '1234567890',
            '123-456-7890',
            '(123) 456-7890',
            '123.456.7890',
        ];

        foreach ($validNumbers as $number) {
            $this->assertTrue($this->rule->passes('phone', $number));
        }
    }

    /** @test */
    public function it_fails_invalid_phone_numbers()
    {
        $invalidNumbers = [
            '12345678901',
            'klasjd lksajd',
            '123-456-353-7890',
        ];

        foreach ($invalidNumbers as $number) {
            $this->assertFalse($this->rule->passes('phone', $number));
        }
    }
}
