<section id="contact" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>Contact Guy Smiley</h2>
            <p>Remember Guy Smiley?  Yeah, he wants to hear from you.</p>
            <div id="success" style="display:none"></div>
            <button id="reload-form" class="btn btn-default btn-lg" style="display:none">
                Send Another Message
            </button>
            <form id="contact-form" class="form-horizontal">
                <div class="form-group" id="name-container">
                    <label for="name" class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" placeholder="Full Name" required />
                        <span class="help-block" id="name-error" style="display:none;"></span>
                    </div>
                </div>
                <div class="form-group" id="email-container">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input name="email" type="email" class="form-control" placeholder="Email Address" required />
                        <span class="help-block" id="email-error" style="display:none;"></span>
                    </div>
                </div>
                <div class="form-group" id="phone-container">
                    <label for="phone" class="col-sm-2 control-label">Phone Number</label>
                    <div class="col-sm-10">
                        <input name="phone" type="tel" class="form-control" placeholder="Phone Number (Optional)" />
                        <span class="help-block" id="phone-error" style="display:none;"></span>
                    </div>
                </div>
                <div class="form-group" id="message-container">
                    <label for="message" class="col-sm-2 control-label">Message</label>
                    <div class="col-sm-10">
                        <textarea name="message" class="form-control" rows="4" required></textarea>
                        <span class="help-block" id="message-error" style="display:none;"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <div id="form-error" class="alert alert-danger" style="display: none;"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button type="submit" id="contact-submit" class="btn btn-default btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@section('custom-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#contact-form').submit(function(e) {
            e.preventDefault();
            $('#contact-submit').prop('disabled', true);
            clearErrors();

            $.post('/api/emails', $(this).serialize())
            .done(function(resp) {
                if (typeof resp.status !== 'undefined' && resp.status === 'success') {
                    // Successful submission
                    $('#contact-form').hide();
                    $('#contact-form :input').val('');
                    $('#success').show().html(`
                        <p>Your message has been received!</p>
                        <p>${resp.message}</p>
                    `);
                    $('#reload-form').show();
                } else {
                    addUnknownError();
                }
            })
            .fail(function(xhr) {
                if (typeof xhr.responseJSON.errors !== 'undefined') {
                    // Validation errors
                    $.each(xhr.responseJSON.errors, function(input, error) {
                        addValidationError(input, error);
                    });
                } else {
                    // Unknown error
                    addUnknownError();
                }
            })
            .always(function() {
                $('#contact-submit').prop('disabled', false);
            });
        });

        $('#reload-form').click(function(e) {
            e.preventDefault();

            $('#contact-form :input').val('');
            $('#contact-form').show();
            $('#success').hide().html('');
            $('#reload-form').hide();
        });

        function addValidationError(name, message) {
            $('#' + name + '-container').addClass('has-error');
            $('#' + name + '-error').text(message).show();
        }

        function addUnknownError() {
            $('#form-error').text('An unknown error occurred. Please refresh the page and try again.').show();
        }

        function clearErrors() {
            $('#form-error').text('').hide();

            $('#name-container').removeClass('has-error');
            $('#name-error').text('').show();
            $('#email-container').removeClass('has-error');
            $('#email-error').text('').show();
            $('#phone-container').removeClass('has-error');
            $('#phone-error').text('').show();
            $('#message-container').removeClass('has-error');
            $('#message-error').text('').show();
        }
    });
</script>
@endsection
