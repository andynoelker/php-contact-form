@extends('layouts.app')

@section('content')

    <!-- Intro Header -->
    @include('pages.home.sections.intro')

    <!-- About Section -->
    @include('pages.home.sections.about')

    <!-- Coffee Section -->
    @include('pages.home.sections.coffee')

    <!-- Contact Section -->
    @include('pages.home.sections.contact')

    <!-- Map Section -->
    @include('pages.home.sections.map')
@endsection
