<!DOCTYPE html>
<html lang="en">

@include('partials.head')

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    @include('partials.nav')

    @yield('content')

    @include('partials.footer')
    @include('partials.scripts')
</body>

</html>
