<h3>Someone reached out through the contact form. Details are below.</h3>
<p>Name: {{ $email->name }}</p>
<p>Email: {{ $email->email }}</p>
@if ($email->phone != '')
<p>Phone: {{ $email->phone }}</p>
@endif
<p>Message:</p>
<p>{!! nl2br($email->message) !!}</p>
